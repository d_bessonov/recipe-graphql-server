class CreateRecipes < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes do |t|
      t.string :name, null: false
      t.string :time
      t.string :ingredients
      t.string :directions

      t.timestamps
    end
  end
end
