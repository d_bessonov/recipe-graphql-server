class CreateFavorites < ActiveRecord::Migration[6.0]
  def change
    create_table :favorites do |t|

      t.timestamps
    end

    add_reference :favorites, :user, foreign_key: true
    add_reference :favorites, :recipe, foreign_key: true
  end
end
