class Recipe < ApplicationRecord
    has_many :comments, dependent: :destroy
    belongs_to :user
    has_and_belongs_to_many :categories
    has_many :favorites, dependent: :destroy
    has_one_base64_attached :image
end
