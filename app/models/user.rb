class User < ApplicationRecord
    has_many :comments, dependent: :destroy
    has_many :recipes, dependent: :destroy
    has_many :favorites, dependent: :destroy
end
