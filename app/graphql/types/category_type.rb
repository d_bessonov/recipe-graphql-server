module Types
  class CategoryType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :description, String, null: false
    field :recipes, [RecipeType], null: false
    field :image_url, String, null: true
  end
end
