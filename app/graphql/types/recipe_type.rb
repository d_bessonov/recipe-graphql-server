module Types
  class RecipeType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :time, String, null: false
    field :ingredients, String, null: false
    field :directions, String, null: false
    field :comments, [CommentType], null: true
    field :user, UserType, null: false
    field :categories, [CategoryType], null: false

    field :image_url, String, null: true

    # def image_url
    #   Rails.application.routes.url_helpers
    #        .rails_blob_url(recipe.image, only_path: true)
    # end
  end
end
