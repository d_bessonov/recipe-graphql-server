module Types
  class CommentType < Types::BaseObject
    field :content, String, null: false
    field :user, UserType, null: false
  end
end
