module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :username, String, null: false
    field :recipes, [RecipeType], null: true
    field :favorites, [FavoriteType], null: true
  end
end
