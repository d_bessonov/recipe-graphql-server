module Types
  class FavoriteType < Types::BaseObject
    field :recipe, RecipeType, null: false
    field :user, UserType, null: false
  end
end
