module Types
    class IndexType < Types::BaseObject
      field :recipes, [RecipeType], null: false
      field :categories, [CategoryType], null: false
    end
  end
  