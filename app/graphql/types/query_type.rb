module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :all_users, [UserType], null: false
    field :all_recipes, [RecipeType], null: false
    field :all_categories, [CategoryType], null: false

    field :recipe, RecipeType, null: false do
      description 'Returns single recipe'
      argument :id, ID, required: true
    end

    field :user, UserType, null: false do
      description 'Returns single user'
      argument :id, ID, required: true
    end

    field :category, CategoryType, null: false do
      description 'Returns single category'
      argument :id, ID, required: true
    end

    def all_users
      User.all
    end

    def user(id:)
      User.find id
    end

    def all_recipes
      Recipe.all
    end

    def recipe(id:)
      Recipe.find id
    end

    def all_categories
      Category.all
    end

    def category(id:)
      Category.find id
    end

  end
end
