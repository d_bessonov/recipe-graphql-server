module Mutations
  class CreateRecipe < BaseMutation
    # arguments passed to the `resolve` method
    argument :name, String, required: true
    argument :time, String, required: true
    argument :ingredients, String, required: true
    argument :directions, String, required: true
    argument :categories_id, [ID], required: false
    argument :image, String, required: false

    # return type from the mutation
    type Types::RecipeType

    def resolve(name: nil, time: nil, ingredients: nil, directions: nil, categories_id: nil, image: nil)
      currentuser = User.first
      recipe = Recipe.create!(
        name: name,
        time: time,
        ingredients: ingredients,
        directions: directions,
        user: currentuser, 
      )
      if image
        recipe.image.attach(data: image)
      end
      recipe.update(
        image_url: Rails.application.routes.url_helpers.rails_blob_path(recipe.image, only_path: true)
      )
      if categories_id.any?
        for idValue in categories_id do
          recipe.categories << Category.find(idValue)
        end
      end
    end
  end
end