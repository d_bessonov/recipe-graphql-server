module Mutations
  class CreateCategory < BaseMutation
    # arguments passed to the `resolve` method
    argument :name, String, required: true
    argument :description, String, required: true

    argument :image, String, required: false

    # return type from the mutation
    type Types::CategoryType

    def resolve(name: nil, description: nil, image: nil)
      category = Category.create!(
        name: name,
        description: description,
      )
      if image
        category.image.attach(data: image)

        category.update(
          image_url: Rails.application.routes.url_helpers.rails_blob_path(category.image, only_path: true)
        )
      end
    end
  end
end